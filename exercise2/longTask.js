const someVeryLongTask = () => {
    /**
     * Simulating a long running task. No need to optimize this.
     */
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Long task completed')
            resolve('Task result');
        }, 10000);
    });
}


module.exports = {
    someVeryLongTask
}