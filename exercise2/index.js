const { connectToDatabase } = require('./db');
const { someVeryLongTask } = require('./longTask');

module.exports.largeFunction = async () => {
  const dbConn = await connectToDatabase()
  const taskResult = await someVeryLongTask()

  console.log({
    dbConn,
    taskResult
  })

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Function was executed'
      },
      null,
      2
    ),
  };
};
