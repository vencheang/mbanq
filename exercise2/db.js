const connectToDatabase = () => {
    /**
     * Simulating a delay for the connection. No need to optimize this.
     */
    console.log('Connecting to the database...');

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const isConnected = true;

            if (isConnected) {
                console.log('Connected to the database!');
                resolve('DB Connection');
            } else {
                console.log('Failed to connect to the database.');
                reject(new Error('Failed to connect to the database.'));
            }
        }, 2000);
    });
}


module.exports = {
    connectToDatabase
}