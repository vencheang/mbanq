This example I've enabled caching to improve performance for

function: largeFunction
endpoint: /

By using serverless-api-gateway-caching plugin
serverless able to cache the following request during this 3600 seconds window
To see an effect you can manually change enabled option to false

caching:
enabled: false

in serverless.yml file
