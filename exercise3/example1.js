function outerFunction() {
  let count = 0;

  function innerFunction() {
    // This variable is still accessible in the inner function,
    // even though it was declared in the outer function.
    console.log(count);
    count++;
  }

  return innerFunction;
}

const innerFunction = outerFunction();

innerFunction(); // 0
innerFunction(); // 1
