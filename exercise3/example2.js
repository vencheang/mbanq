// A closure that generates a random number.
function randomNumber() {
  let randomNumber = Math.random();

  function getRandomNumber() {
    return randomNumber;
  }

  return getRandomNumber;
}

const getRandomNumberFunction = randomNumber();
console.log(getRandomNumberFunction()); // A random number between 0 and 1
